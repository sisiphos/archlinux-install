PARTITION_TYPE_LINUX='0FC63DAF-8483-4772-8E79-3D69D8477DE4'
PARTITION_TYPE_SWAP='0657FD6D-A4AB-43C4-84E5-0933C84B4F4F'
PARTITION_TYPE_UEFI='C12A7328-F81F-11D2-BA4B-00A0C93EC93B'

function f_get_partition_device {
  local block_dev=$1
  local part_num=$2

  if [[ ${(U)block_dev} = "/DEV/NVME"* ]]
  then
      echo "${block_dev}p$part_num"
  else
      echo "$block_dev$part_num"
  fi
}

function r_get_or_create_partition {
  local partnum_varname="$1_NUM"
  local partname_varname="$1_NAME"
  local partsize_varname="$1_SIZE"
  local parttype_code="${(U)2}"

  assert_not_empty $partnum_varname

  local partnum=${(P)partnum_varname}
  local partname=${(P)partname_varname}

  local part_dev=$(f_get_partition_device $BLOCKDEV_INSTALL $partnum)

  if [ $parttype_code = "LINUX" ]
  then
    local parttype=$PARTITION_TYPE_LINUX
  elif [ $parttype_code = "SWAP" ]
  then
    local parttype=$PARTITION_TYPE_SWAP
  elif [ $parttype_code = "UEFI" ]
  then
    local parttype=$PARTITION_TYPE_UEFI
  else
    fail "Invalid partition type '$2' for partition $part_dev" 5
  fi

  if [ ! -e $part_dev ]
  then
    assert_not_empty $partsize_varname
    local partsize=${(P)partsize_varname}
    debug "Create partition $part_dev."
    echo -e "$part_dev: size=$partsize,type=$parttype,name=$partname" | sfdisk --append --quiet $BLOCKDEV_INSTALL
    exit_if_failed "Error creating partition $part_dev."
  else
      local current_parttype=$( sfdisk --part-type $BLOCKDEV_INSTALL $partnum )
      if [ ${current_parttype:u} != ${parttype:u} ]
      then
        fail "Existing partition $partdev has wrong type $current_parttype." 5
      fi
      local current_partname=$( sfdisk --part-label $BLOCKDEV_INSTALL $partnum )
      if [ ${current_partname:u} != ${partname:u} ]
      then
        debug "Changing name of partition $part_dev from '$current_partname' to '$partname'"
        sfdisk --quiet --part-label $BLOCKDEV_INSTALL $partnum $partname
        exit_if_failed "Error renaming partition $part_dev."
      fi
   fi

   RETVAL=$part_dev
}

function format_boot {
  local boot_dev=$1

  require_dev $boot_dev
  debug "format boot partition $boot_dev"
  mkfs.ext4 -qF -L arch_boot $boot_dev
  exit_if_failed "Formatting boot partition failed."
}

function format_swap {
  local swap_dev=$1

  require_dev $swap_dev
  debug "format/create swap partition $swap_dev"
  mkswap -L arch_swap -qf $swap_dev
  exit_if_failed "Formatting swap partition failed."
}

function format_uefi {
  local uefi_dev=$1

  require_dev $uefi_dev
  debug "format UEFI partition $uefi_dev"
  mkfs.vfat -n "EFI System Partition" $uefi_dev
  exit_if_failed "Formatting EFI partition failed."
}

function create_and_format_encrypted_root {
  local luks_dev=$1
  local luks_pass=$2

  require_dev $luks_dev
  debug "setup LUKS in partition $luks_dev"
  modprobe dm-mod dm-crypt
  echo "$luks_pass" | cryptsetup luksFormat $luks_dev -d -
  exit_if_failed "Setting up LUKS partition failed."
  debug "open LUKS partition"
  echo "$luks_pass" | cryptsetup open $luks_dev luksroot -d -
  exit_if_failed "Opening LUKS partition failed."
  debug "format LUKS partition to ext4"
  mkfs.ext4 -qF -L arch_root /dev/mapper/luksroot
  exit_if_failed "Formatting LUKS partition failed."
}

