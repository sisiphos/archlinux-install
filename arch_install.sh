#!/usr/bin/env zsh

SCRIPT_DIR="$(dirname "${BASH_SOURCE}")"

source "$SCRIPT_DIR/functions.inc.sh"
source "$SCRIPT_DIR/functions_disk.inc.sh"
source "$SCRIPT_DIR/functions_pacman.inc.sh"

VERBOSE_OUTPUT=true

CPU_ARCH= # AMD or INTEL
DISABLE_KERNEL_IBT=false

PART_BOOT_NUM='2'
PART_BOOT_NAME='ARCH_BOOT'
PART_BOOT_SIZE='384M'

PART_SWAP_NUM='3'
PART_SWAP_NAME='ARCH_SWOPP'
PART_SWAP_SIZE='12G'

PART_ROOT_NUM='4'
PART_ROOT_NAME='ARCH_LUKS_ROOT'
PART_ROOT_SIZE='+'	# '+' means remaining space

PART_UEFI_NUM='1'
PART_UEFI_NAME="PART_EFI"
PART_UEFI_SIZE='100M'

BLOCKDEV_INSTALL='/dev/sda'
BLOCKDEV_UEFI='/dev/sda'

PART_ROOT_PASS='equinox.hoss.coffee.bladder'

### check variables

step "Checking prerequisites."

debug "check config variables"

assert_not_empty 'BLOCKDEV_INSTALL'
require_dev $BLOCKDEV_INSTALL

### functions

### MAIN

debug "check UEFI64"
if [ $(cat /sys/firmware/efi/fw_platform_size) -ne '64' ]
then
  fail "No UEFI64 detected." 2
fi

debug "check internet connection"
if ! nc -zw1 archlinux.org 443
then
  fail "No internet connection." 3
fi

step "Prepare disk."

debug "partitioning"
r_get_or_create_partition 'PART_UEFI' UEFI && DEV_UEFI=$RETVAL
r_get_or_create_partition 'PART_BOOT' LINUX && DEV_BOOT=$RETVAL
r_get_or_create_partition 'PART_SWAP' LINUX && DEV_SWAP=$RETVAL
r_get_or_create_partition 'PART_ROOT' LINUX && DEV_LUKS=$RETVAL

format_boot $DEV_BOOT
format_swap $DEV_SWAP
create_and_format_encrypted_root $DEV_LUKS

debug "Mounting essential devices"
mount /dev/mapper/luksroot /mnt
cd /mnt
mkdir boot
mount $DEV_BOOT /mnt/boot
mkdir boot/efi
mount $DEV_UEFI /mnt/boot/efi

