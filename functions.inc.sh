COL_RED='\033[1;31m'
COL_BLUE='\033[1;34m'
COL_WHITE='\033[1;37m'
COL_GREEN='\033[1;32m'
COL_YELLOW='\e[1,33m'
COL_NONE='\033[0m'
COL_GRAY='\e[2;37m'

function error {
  echo -e "[${COL_RED}FAIL$COL_NONE] $1" >/dev/stderr
}

function success {
  echo -e "[${COL_GREEN}Success$COL_NONE] $1"
}

function fail {
  exit_code=${2:-7}  # coalesce exit code with default of 7

  error "$1"
  exit $exit_code
}

function warn {
  echo -e "[${COL_YELLOW}Warning$COL_NONE] $1"
}

function step {
  echo -e "\n$COL_WHITE$1$COL_NONE"
}

function info {
  echo -e "$COL_NONE$1"
}

function debug {
  if [[ "$VERBOSE_OUTPUT" = true ]]
  then
    echo -e "[${COL_BLUE}TRACE$COL_NONE] $COL_GRAY$1$COL_NONE"
  fi
}

function exit_if_failed {
  retval=$?
  if [ $retval -ne 0 ]
  then
    error "$1"
    exit $retval
  fi
}

function require_dev() {
  if [ ! -e $1 ]
  then
    fail "Partition $1 not exists." 5
  fi
}

function assert_not_empty() {
  if [ -z ${(P)1} ]
  then
    fail "Expected variable \$$1 to be non-empty." 1
  fi
}


