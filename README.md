# ArchLinux Install Script

A shell script to automate installation of ArchLinux

- [x] check for UEFI 64bit
- [x] check for Internet reachability of `archlinux.org`
- [x] create partitions (boot, root, swap and optionally EFI)
- [x] format EFI partition
- [x] format boot partition
- [x] create/format swap partition
- [x] format/create encrypted root partition with LUKS
- [x] mount boot/root/EFI
- [x] optionally prepare mirrorlist
- [ ] pacstrap packages
- [ ] activate swap
- [ ] create fstab
- [ ] arch-chroot
- [ ] user and passwd
- [ ] timezone, time and date
- [ ] locale and language
- [ ] hostname and hosts
- [ ] pacman.conf
- [ ] dhcpcd service
- [ ] prepare grub default (for encrypted root)
- [ ] prepare mkinitcpio.conf then create
- [ ] prepare grub for NVIDIA GPU with Intel CPU (optional)
- [ ] prepare grub for actual microcode (kernel parameter)
- [ ] prepare grub for graphics console and resolution (optional)
- [ ] install grub
- [ ] prepare post-reboot-script
- [ ] exit chroot and reboot

