function retrieve_pacman_mirrors {
  local country=$1

  debug "Deleting current pacman mirrorlist"
  mv -f /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.orig

  debug "Installing package 'pacman-contrib'"
  pacman -Syq --noconfirm pacman-contrib >/dev/null

  if [[ $? -eq 0 ]]
  then
    debug "Loading filtered and ranked pacman mirrorlist for $country"
    curl -o /etc/pacman.d/mirrorlist \
      -s "https://archlinux.org/mirrorlist/?country=$country&protocol=https&use_mirror_status=on" | \
      sed -e 's/^#Server/Server/' -e '/^#/d' | \
      rankmirrors -n 8 -
  else
    debug "Loading generic pacman mirrorlist for $country"
    curl -o /etc/pacman.d/mirrorlist \
      -s "https://archlinux.org/mirrorlist/?country=$country&protocol=https&use_mirror_status=on"
  fi
}

function install_base {
  local architecture=$1
  debug "Installing base packages with 'pacstrap'"
  
  local ucode=""
  if [ ${(U)architecture} = "AMD" ]
  then
    ucode="amd-ucode"
  elif [ ${(U)architecture} = "INTEL" ]
  then 
    ucode="intel-ucode"
  fi

  mount | grep '/mnt ' >/dev/null
  exit_if_failed "Directory /mnt is not mounted."

  pacstrap -K /mnt base base-devel linux linux-firmware \
      bash-completion vim man-db \
      efibootmgr grub os-prober dhcpcd \
      "$ucode"
  exit_if_failed "Installing base packages failed."
}
